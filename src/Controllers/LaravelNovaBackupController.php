<?php

namespace Lex10000\LaravelNovaBackup\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Providers\LaravelNovaBackupServiceProvider;
use Lex10000\LaravelNovaBackup\Support\Utils;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LaravelNovaBackupController extends Controller
{
    protected string $model;

    public function __construct()
    {
        $this->model = LaravelNovaBackupServiceProvider::determineModel();
    }

    /**
     * Загрузить дамп базы (или архив с сайтом) на локальную машину
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse|null
     */
    public function download(Request $request)
    {
        if (!$request->filled('hash') || !Utils::checkedHash($request->input('hash'))) {
            throw new HttpException(403);
        }

        $backup = $this->model::query()
            ->where('type', $request->input('type', BackupType::DATABASE))
            ->latest()
            ->first();

        if (!$backup || !File::exists(config('laravel-nova-backup.path') . $backup->filename)) {
            throw new HttpException(404);
        }

        return response()->download(
            config('laravel-nova-backup.path') . $backup->filename,
            $backup->database_name,
            [
                'Content-Type' => 'application/octet-stream',
                'Cache-Control' => 'no-cache'
            ]
        );
    }
}
