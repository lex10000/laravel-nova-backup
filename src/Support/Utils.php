<?php
namespace Lex10000\LaravelNovaBackup\Support;


use Illuminate\Support\Facades\Hash;

class Utils
{
    /**
     * Сверяет хеш, переданный по url с хешом из конфига
     * @param string $hash
     * @return bool
     */
    public static function checkedHash(string $hash): bool
    {
        if (!config('laravel-nova-backup.download_password')) {
            return false;
        }

        return (Hash::check(config('laravel-nova-backup.download_password'), $hash));
    }
}
