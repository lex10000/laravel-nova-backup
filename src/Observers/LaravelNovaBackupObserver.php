<?php

namespace Lex10000\LaravelNovaBackup\Observers;

use Illuminate\Support\Facades\File;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup;
use Lex10000\LaravelNovaBackup\Services\BackupService;

class LaravelNovaBackupObserver
{

    /**
     * Handle the Backup "created" event.
     *
     * @param LaravelNovaBackup $backup
     * @return void
     */
    public function creating(LaravelNovaBackup $backup)
    {
        File::ensureDirectoryExists(config('laravel-nova-backup.path'), 0777);
        $filename = BackupService::generateBackupName($backup->type->value);

        if ($backup->type->value === BackupType::FILES) {
            BackupService::createFileArchive($filename, config('laravel-nova-backup.file_path'));
            $backup->filename = $filename;
            return;
        }

        if (BackupService::makeBackup($filename, [], $backup->type->value)['code'] === BackupService::SUCCESS_STATUS_CODE) {
            $backup->filename = $filename;
        }
    }


    /**
     * Handle the Backup "deleted" event.
     *
     * @param LaravelNovaBackup $backup
     * @return void
     */
    public function deleted(LaravelNovaBackup $backup)
    {
        $path = config('laravel-nova-backup.path') . $backup->filename;
        if (File::exists($path)) {
            File::delete($path);
        }
    }
}
