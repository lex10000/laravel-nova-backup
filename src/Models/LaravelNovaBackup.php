<?php

namespace Lex10000\LaravelNovaBackup\Models;

use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Illuminate\Database\Eloquent\Model;

class LaravelNovaBackup extends Model
{
    const DAILY_COMMENT = 'daily';

    protected $table = 'laravel-nova-backups';

    protected $casts = [
      'type' => BackupType::class,
    ];

    protected $fillable = [
      'filename', 'type', 'comment'
    ];
}
