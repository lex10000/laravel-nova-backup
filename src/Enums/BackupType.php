<?php

namespace Lex10000\LaravelNovaBackup\Enums;

use BenSampo\Enum\Enum;

final class BackupType extends Enum
{
    /**
     * Полный бекап базы данных
     */
    const DATABASE = 0;

    /**
     * В бекап входят только определнные таблицы, указанные в конфиге
     */
    const DATABASE_MINI = 1;

    /**
     * Ежедневный бекап (тип такого бекапа указывается в конфиге)
     */
    const DATABASE_DAILY = 4;

    /**
     * Бекап файлов проекта
     */
    const FILES = 2;
}
