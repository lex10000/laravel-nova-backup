<?php

namespace Lex10000\LaravelNovaBackup\Contracts;

/**
 * Реализация исполнения shell команд.
 * Interface CommandShellInterface
 */
interface CommandShellInterface
{

    /**
     * Код успешно выполненной команды.
     */
    const SUCCESS_STATUS_CODE = 0;

    /**
     * Запуск shell команд.
     * @param array $commands команды, которые должны выполниться в shell.
     * @param array $parameters дополнительные параметры, используется если shell команда подразумевает общение с
     * пользователем. Ключ - это строка, которую показывает команда, значение - это то, что нужно ввести в ответ.
     * Например, при бекапировании базы данных, postgres просит ввести пароль. Тогда параметры
     * будут ["Password for user 'USER': " => 'PASSWORD']
     * @return mixed
     */
    public static function run(array $commands, array $parameters = []);

    /**
     * Подготовка комманд для запуска
     * @param array $commands массив комманд
     * @return string
     */
    public static function prepareCommands(array $commands):string;

    /**
     * Подготовка результата выполнения команд.
     * @return array
     */
    public static function sendResult():array;
}
