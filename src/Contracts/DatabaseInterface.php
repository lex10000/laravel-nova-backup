<?php

namespace Lex10000\LaravelNovaBackup\Contracts;


use Lex10000\LaravelNovaBackup\Enums\BackupType;

/**
 * Interface DatabaseInterface для работы с sql базами данных.
 */
interface DatabaseInterface
{
    /**
     * Создание дампа базы данных
     * @param string $filename название файла (полное)
     * @param array $tables список таблиц для доабвления в бекап (если не передан - то берем список из конфига)
     * @param int $type
     * @return mixed
     */
    public static function makeBackup(string $filename, array $tables = [], int $type = BackupType::DATABASE);

    /**
     * Восстановление дампа базы данных.
     * @param string $filename файл дамп базы данных.
     * @return mixed
     */
    public static function restoreBackup(string $filename);

    /**
     * Получить необходимые параметры подключения к бд
     * @return array
     */
    public static function getDbConfig(): array;
}
