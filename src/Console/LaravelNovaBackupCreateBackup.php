<?php
namespace Lex10000\LaravelNovaBackup\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup;

class LaravelNovaBackupCreateBackup extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'laravel-nova-backup:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create backup';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $backup = LaravelNovaBackup::query()->create(
                [
                    'comment' => config('laravel-nova-backup.daily_backup_type_is_mini') ? 'mini' : 'default',
                    'type' => BackupType::DATABASE_DAILY
                ]
            );
            $this->info('Backup was created successfully. Type: '.$backup->comment);
        } catch (\Throwable $e) {
            $this->error('ups');
        }
    }
}
