<?php
namespace Lex10000\LaravelNovaBackup\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class LaravelNovaBackupGenerateSecretCommand extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'laravel-nova-backup:secret';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the Laravel nova backup secret password used to download database backup from
    production server';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $key = Str::random();

        if (!file_exists($path = $this->envPath())) {
            return $this->displayKey($key);
        }

        if (Str::contains(file_get_contents($path), 'LARAVEL_NOVA_BACKUP_DOWNLOAD_PASSWORD') === false) {
            // create new entry
            file_put_contents($path, PHP_EOL."LARAVEL_NOVA_BACKUP_DOWNLOAD_PASSWORD=$key".PHP_EOL, FILE_APPEND);
        } else {
            // update existing entry
            file_put_contents($path, str_replace(
                'LARAVEL_NOVA_BACKUP_DOWNLOAD_PASSWORD='.$this->laravel['config']['laravel-nova-backup.download_password'],
                'LARAVEL_NOVA_BACKUP_DOWNLOAD_PASSWORD='.$key, file_get_contents($path)
            ));
        }

        $this->displayKey($key);
    }

    /**
     * Get the .env file path.
     *
     * @return string
     */
    protected function envPath()
    {
        if (method_exists($this->laravel, 'environmentFilePath')) {
            return $this->laravel->environmentFilePath();
        }

        return $this->laravel->basePath('.env');
    }
    /**
     * Display the key.
     *
     * @param  string  $key
     *
     * @return void
     */
    protected function displayKey($key)
    {
        $this->laravel['config']['laravel-nova-backup.download_password'] = $key;

        $this->info("Laravel nova backup secret password [$key] set successfully.");
    }
}
