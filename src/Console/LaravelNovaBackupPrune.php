<?php

namespace Lex10000\LaravelNovaBackup\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup;

class LaravelNovaBackupPrune extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'laravel-nova-backup:prune';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prune backups';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $count = 0;
        try {
            LaravelNovaBackup::query()
                ->where('created_at', '<=', $this->getPruningPeriod())
                ->where('type', BackupType::DATABASE_DAILY)
                ->each(
                    function ($backup) {
                        $backup->delete();
                    }
                );
            $this->info('Backups was pruned successfully. Count: ' . $count);
        } catch (\Throwable $e) {
            $this->error('ups');
        }
    }

    protected function getPruningPeriod()
    {
        return now()->subDays(config('laravel-nova-backup.delete_backups_after_days', 14));
    }
}
