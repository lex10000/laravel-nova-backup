<?php

namespace Lex10000\LaravelNovaBackup\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Lex10000\LaravelNovaBackup\Services\BackupService;

class RestoreBackup extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $path = $models->first()->filename;
        $result = BackupService::restoreBackup($path);
        if ($result['code'] === BackupService::SUCCESS_STATUS_CODE) {
            return Action::message('Бекап успешно восстановлен!');
        }
    }
}
