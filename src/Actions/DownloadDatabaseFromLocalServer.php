<?php

namespace Lex10000\LaravelNovaBackup\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\File;
use Lex10000\LaravelNovaBackup\Services\BackupService;

class DownloadDatabaseFromLocalServer extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        DB::commit();

        /** @var UploadedFile $file */
        if ($file = $fields['file']) {
            $name = $file->getClientOriginalName();
            Storage::disk('backup')->putFileAs('', $file, $name);
            BackupService::restoreBackup($name);
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            File::make('file')->disk('backup')->rules('file','mimes:txt', 'required')->required(),
        ];
    }
}
