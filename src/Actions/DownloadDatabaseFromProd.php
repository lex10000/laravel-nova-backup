<?php

namespace Lex10000\LaravelNovaBackup\Actions;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Services\BackupService;
use Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup;

class DownloadDatabaseFromProd extends Action
{
    public function name()
    {
        return __('Download database from production server');
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $filename = BackupService::generateBackupName();
        $path = config('laravel-nova-backup.path') . $filename;

        if (!config('laravel-nova-backup.download_password')) {
            return Action::danger('Вы не задали пароль для скачивания базы с прода.');
        }

        $hashPass = Hash::make(config('laravel-nova-backup.download_password'));
        $response = Http::timeout(60)
            ->post(config('laravel-nova-backup.download_url'), [
                'hash' => $hashPass
            ]);

        if ($response->status() === 403) {
            return Action::danger('Пароли не совпадают.');
        }

        if ($response->status() === 404) {
            return Action::danger('На Production сервере еще не создано ни одного бекапа.');
        }

        if (!Str::startsWith($response->body(), "--\n-- PostgreSQL database dump")) {
            return Action::danger('Запрос на получение бекапа вернул некорректный результат.');
        }

        $fp = fopen($path, 'w+');

        if ($fp === false) {
            return Action::danger('Не удается открыть файл для сохранения бекапа.');
        }

        fwrite($fp, $response->body());
        fclose($fp);

        $backup = new LaravelNovaBackup();
        $backup->fill(
            [
                'type' => BackupType::DATABASE,
                'filename' => $filename
            ]
        );
        $backup->saveQuietly();
        return Action::message('Бекап был успешно скачан!');
    }
}
