<?php

namespace Lex10000\LaravelNovaBackup;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Resource;
use Lex10000\LaravelNovaBackup\Actions\DownloadDatabaseFromLocalServer;
use Lex10000\LaravelNovaBackup\Actions\DownloadDatabaseFromProd;
use Lex10000\LaravelNovaBackup\Actions\RestoreBackup;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Lex10000\LaravelNovaBackup\Providers\LaravelNovaBackupServiceProvider;
use SimpleSquid\Nova\Fields\Enum\Enum;

class LaravelNovaBackup extends Resource
{
    public static $model = \Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup::class;

    public static $globallySearchable = false;
    public static $search = false;
    public static $searchable = false;

    /**
     * Label for display.
     *
     * @return string
     */
    public static function label()
    {
        return __('Laravel nova backups');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Laravel nova backup');
    }

    /**
     * Get a fresh instance of the model represented by the resource.
     *
     * @return mixed
     */
    public static function newModel()
    {
        self::$model = LaravelNovaBackupServiceProvider::determineModel();

        return new self::$model();
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id'),
            Enum::make('Type')->attach(BackupType::class)->rules('required'),
            Text::make('Filename', 'filename')->hideWhenCreating()->hideWhenUpdating(),
            Text::make(__('Comment'), 'comment')
                ->help(__('This field is not required'))
                ->nullable()
                ->rules('nullable', 'string', 'max:254'),
            DateTime::make('Created at')->hideWhenCreating()->hideWhenUpdating(),
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            DownloadDatabaseFromProd::make()->standalone()->onlyOnIndex()->canSee(fn() => app()->isLocal()),
            RestoreBackup::make()->onlyOnTableRow()->canSee(function () {
                return $this->model()->type ? $this->model()->type->value !== BackupType::FILES: true;
            }),
            DownloadDatabaseFromLocalServer::make()->standalone()->onlyOnIndex(),
        ];
    }

    public static function redirectAfterCreate(NovaRequest $request, $resource)
    {
        return '/resources/'.static::uriKey();
    }
}
