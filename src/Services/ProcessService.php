<?php

namespace Lex10000\LaravelNovaBackup\Services;

use Illuminate\Support\Facades\Log;
use Lex10000\LaravelNovaBackup\Contracts\CommandShellInterface;
use Symfony\Component\Process\InputStream;
use Symfony\Component\Process\Process;

/**
 * Класс, реализующий @see CommandShellInterface для работы с shell командами.
 * Данный класс необходимо наследовать всем классам, которые будут пользоваться shell командами.
 * Class ProcessService
 * @package App\Services\SystemServices
 */
abstract class ProcessService implements CommandShellInterface
{
    protected static Process $process;

    /**
     * @param array $commands
     * @param array $parameters для установки в env окружение процесса (например, пароль для базы данных)
     * @param int $timeout
     * @return array|mixed
     */
    public static function run(array $commands, array $parameters = [], int $timeout = 60)
    {
        $commands = static::prepareCommands($commands);

        static::$process = Process::fromShellCommandline($commands);
        static::$process->setTimeout($timeout);
        static::$process->setEnv($parameters);
        static::$process->start();

        while (static::$process->isRunning()) {
            if ((time() - static::$process->getStartTime()) > static::$process->getTimeout()) {
                static::$process->stop();
                break;
            }
        }

        static::$process->wait();
        Log::channel('process')->debug("Результат выполнения: " . static::$process->getErrorOutput() . static::$process->getOutput());

        return static::sendResult();
    }

    public static function prepareCommands(array $commands): string
    {
        return implode(' && ', $commands);
    }

    /**
     * Результат вывода процесса
     * @return array
     */
    public static function sendResult(): array
    {
        return [
            'code' => static::$process->getExitCode(),
            'message' => 'stdOut: '.static::$process->getOutput()."\nErrOut: ".static::$process->getErrorOutput()
        ];
    }
}
