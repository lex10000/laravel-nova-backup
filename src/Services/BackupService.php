<?php

namespace Lex10000\LaravelNovaBackup\Services;

use Lex10000\LaravelNovaBackup\Enums\BackupType;

class BackupService extends PostgresService
{
    public const DUMP_FORMAT = '.sql.dump';
    public const DUMP_MINI_FORMAT = '.mini.sql.dump';
    public const DUMP_DAILY_FORMAT = '.daily.sql.dump';
    public const ARCHIVE_FORMAT = '.tar.gz';


    /**
     * Создание имени файла для дампа базы данных
     * @param int $type
     * @return string
     */
    public static function generateBackupName($type = BackupType::DATABASE)
    {
        $namePrefix = time();
        switch ($type) {
            case BackupType::DATABASE:
                $name = $namePrefix . static::DUMP_FORMAT;
                break;
            case BackupType::FILES:
                $name = $namePrefix . static::ARCHIVE_FORMAT;
                break;
            case BackupType::DATABASE_MINI :
                $name = $namePrefix . static::DUMP_MINI_FORMAT;
                break;
            case BackupType::DATABASE_DAILY :
                $name = $namePrefix . static::DUMP_DAILY_FORMAT;
                break;
            default:
                $name = $namePrefix;
                break;
        }
        return $name;
    }

    public static function createFileArchive($name, $sitePath)
    {
        $archivePath = config('laravel-nova-backup.path') . $name;
        static::run(
            [
                "tar -cf $archivePath $sitePath",
            ]
        );
    }
}
