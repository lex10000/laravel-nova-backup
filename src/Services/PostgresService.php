<?php

namespace Lex10000\LaravelNovaBackup\Services;

use Illuminate\Support\Facades\DB;
use Lex10000\LaravelNovaBackup\Contracts\DatabaseInterface;
use Lex10000\LaravelNovaBackup\Enums\BackupType;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Log;

class PostgresService extends ProcessService implements DatabaseInterface
{
    public static function makeBackup(string $filename, array $tables = [], int $type = BackupType::DATABASE)
    {
        $config = config('laravel-nova-backup');

        Log::channel($config['log_channel'])->debug("start to create backup with name $filename");

        $path = $config['path'] . $filename;

        $result_tables = null;

        switch ($type) {
            case BackupType::DATABASE_MINI:
                $result_tables = static::prepareTables($config['tables']);
                break;
            case BackupType::DATABASE_DAILY:
                $result_tables = $config['daily_backup_type_is_mini'] ? static::prepareTables($config['tables']) : null;
                break;
        }

        if ($tables) {
            $result_tables = static::prepareTables($tables);
        }

        [$username, $host, $database, $password] = static::getDbConfig();


        return static::run(
            [
                "pg_dump --clean -d $database -U $username -h $host $result_tables --file=$path",
            ],
            [
                'PGPASSWORD' => $password,
            ]
        );
    }

    public static function restoreBackup(string $filename)
    {
        $config = config('laravel-nova-backup');

        Log::channel($config['log_channel'])->debug("start to restore backup with name $filename");

        $path = $config['path'] . $filename;

        if (!file_exists($path) || !filesize($path)) {
            Log::channel($config['log_channel'])->debug('Ошибка при восстановлении бекапа. Файл ' . $path . 'не найден или поврежден.');
            throw new HttpException('500', 'Ошибка при восстановлении бекапа. Файл не найден или поврежден.');
        }

        [$username, $host, $database, $password] = static::getDbConfig();


        return static::run(
            [
                "psql -d $database -U $username -h $host --file=$path",
            ],
            [
                'PGPASSWORD' => $password,
            ]
        );
    }

    /**
     * Получить необходимые параметры подключения к бд
     * @return array
     */
    public static function getDbConfig(): array
    {
        return [
            DB::connection()->getConfig('username'),
            DB::connection()->getConfig('host'),
            DB::connection()->getConfig('database'),
            DB::connection()->getConfig('password')
        ];
    }

    /**
     * Подготовка таблиц для добавления в запрос
     * @param array $tables
     * @return string
     */
    protected static function prepareTables(array $tables): string
    {
        return ' -t ' . join(' -t ', $tables);
    }
}
