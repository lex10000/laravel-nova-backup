<?php

namespace Lex10000\LaravelNovaBackup\Providers;

use http\Exception\InvalidArgumentException;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;
use Lex10000\LaravelNovaBackup\Console\LaravelNovaBackupCreateBackup;
use Lex10000\LaravelNovaBackup\Console\LaravelNovaBackupGenerateSecretCommand;
use Lex10000\LaravelNovaBackup\Console\LaravelNovaBackupPrune;
use Lex10000\LaravelNovaBackup\Models\LaravelNovaBackup;
use Lex10000\LaravelNovaBackup\Observers\LaravelNovaBackupObserver;

class LaravelNovaBackupServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $configPath = realpath(__DIR__ . '/../../config/laravel-nova-backup.php');
        $this->publishes([
            $configPath => config_path('laravel-nova-backup.php'),
        ], 'config');

        $this->mergeConfigFrom($configPath, 'laravel-nova-backup');

        if (!class_exists('CreateLaravelNovaBackupsTable')) {
            $timestamp = date('Y_m_d_His', time());

            $this->publishes([
                __DIR__ . '/../../migrations/create_laravel-nova-backups_table.php.stub' => database_path("/migrations/{$timestamp}_create_laravel-nova-backups_table.php"),
            ], 'migrations');
        }

        Nova::resources([
            config('laravel-nova-backup.resource', \Lex10000\LaravelNovaBackup\LaravelNovaBackup::class),
        ]);

        LaravelNovaBackup::observe(LaravelNovaBackupObserver::class);

        $this->loadRoutesFrom(realpath(__DIR__.'/../../routes/api.php'));
    }

    public function register()
    {
        $this->commands([
            LaravelNovaBackupGenerateSecretCommand::class,
            LaravelNovaBackupCreateBackup::class,
            LaravelNovaBackupPrune::class,
        ]);

        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            config('laravel-nova-backup.daily_backup_is_enabled') ? $schedule->command('laravel-nova-backup:create')->daily() : null;
            config('laravel-nova-backup.daily_backup_is_enabled') ? $schedule->command('laravel-nova-backup:prune')->daily() : null;
        });
    }

    public static function determineModel(): string
    {
        $model = config('laravel-nova-backup.model') ?? LaravelNovaBackup::class;

        if (!is_a($model, LaravelNovaBackup::class, true)
            || !is_a($model, Model::class, true)) {
            throw new InvalidArgumentException("The given model class $model does not implement " . LaravelNovaBackup::class);
        }

        return $model;
    }
}
