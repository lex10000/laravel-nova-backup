<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api/backup')->group(function () {

    /**
     * Загрузка последнего бекапа с prod-сервера (для последующего восстановления на локальном сервере)
     * @param string hash - парольное слово (для аутентификации)
     */
    Route::post('download', [\Lex10000\LaravelNovaBackup\Controllers\LaravelNovaBackupController::class, 'download']);
});
